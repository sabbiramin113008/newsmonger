# README.md

## News Monger

A Personalized newsfeed based on NEWS API and Newsletter using SEND-GRID.

## Environment Preparation

1. Make sure `venv` or any virtual environment is installed. If not, try, `sudo apt-get install python3-venv` and also, please make sure the pip is upgraded to the latest version. If Not, then try `pip install ---upgrade pip`
2. If you are using `venv` make sure to activate it via `source venv/bin/activate`.
3. Make sure to install the exact same version of the modules docked in `requirements.txt`. Type `pip install -r requirements.txt` on terminal and you are ready to go.

## Database and Initial Migrations

1. An SQLite database is added in the project so as to run it without setting up database, admin or other stuff. For this,
    1. Use `sabbir` and `123456` as admin username and password respectively.
2. Run `python manage.py makemigrations` and `python manage.py migrate` to take the make the migrations if setting up new db.
3. Run `python manage createsuperuser` for creating Admin.

## Creating `.ENV` File

Create a .env file as it is being ignored

```json
NEWS_API_KEY=<news-api-key>
SENDGRID_API_KEY=<sendgrid-api-key>
SECRET_KEY=<secret-key-for-django-app>
fetching_news_interval=15
scanning_news_interval=45
sending_mail_interval=6
```

## Setting Up Cronjobs

`apscheduler` is used for scheduled tasks. It is used for fetching news by sources, scanning news titles for search keywords by users and sending emails.

All the tasks can be found in `../news/tasks.py` module. The interval can be set from the `.env`file accordingly.

## Running the Application

1. Make sure db is prepared and `.env` is set up.
2. Run `python manage.py runserver localhost:5003 --noreload` to run the application with all the cronjobs and without restart.

## Application APIs Docs and Screenshots

1. All the APIs can be accessible from HTTP Client.
2. All the Insomnia Sample API Request-Response collections are saved in the `./insomnia_docs` .NB. To Load the file, download Insomnia( HTTP Client Tools) from [here](https://insomnia.rest/download).
3. Also the `screenshots` can be found in `./screenshots` dir.

## Major Components

- `Admin Panel` - is made up of `Django Admin Panel`.
- `Application API Server` - API server for REST interface and REST Integration. `JWT` has been used for authorized API calls for endpoints.
- `Client App` - Basically made with `Bootstrap` and `Django Views`.
- `Cronjobs` - Several cronjobs for actions specific tasks and ease of use.
    - `Fetch News` - By NEWS API from different sources. Also, it fetches the valid sources (country and news sources) while starting up the application.
    - `Preparing Newsletter by Keywords` - User specific newsletter generation by the choices of the users.
    - `Sending Mail` - Mails as Newsletter are sent via `SEND-GRID` .

## Milestones Status

- [x]  Django Admin
- [x]  Django REST Framework
- [x]  User App
    - [x]  Login
    - [x]  Register
    - [x]  News Preference
    - [x]  Personalized Newsfeed
    - [x]  Setting Up Newsletter
- [x]  Crons
    - [x]  News Source Fetcher
    - [x]  News Fetcher
    - [x]  Scanner for Keywords
    - [x]  Sending Mail Cron
- [x]  Insomnia API Docs
- [x]  Screenshots
- [x]  README