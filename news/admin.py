# -*- coding: utf-8 -*-

"""
author: S.M. Sabbir Amin
date: 26 Jun 2021
email: sabbiramin.cse11ruet@gmail.com, sabbir@rokomari.com

"""
from django.contrib import admin

from django.contrib import admin

from news.models import NewsPreference, Source, Country, News, Newsletter

admin.site.site_header = 'NewsMonger 1.0'
admin.site.site_title = 'NewsMonger 1.0'
admin.site.index_title = 'Welcome to NewsMonger Admin 1.0'

admin.site.register(NewsPreference)
admin.site.register(Source)
admin.site.register(Country)
admin.site.register(News)
admin.site.register(Newsletter)

