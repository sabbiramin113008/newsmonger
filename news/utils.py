# -*- coding: utf-8 -*-

"""
author: S.M. Sabbir Amin
date: 27 Jun 2021
email: sabbiramin.cse11ruet@gmail.com, sabbir@rokomari.com

"""
import os
from sendgrid import SendGridAPIClient
from sendgrid.helpers.mail import Mail


class MailUtil:
    from dotenv import load_dotenv
    load_dotenv()
    def __init__(self):
        self.api_key = str(os.environ.get('SENDGRID_API_KEY', '!APIKEY'))

    def send_mail(self, user_mail, subject, html_content):

        from_email = 'admin@newsmonger.com'
        message = Mail(

            from_email=from_email,
            to_emails=user_mail,
            subject=subject,
            html_content=html_content)
        try:
            sg = SendGridAPIClient(os.environ.get('SENDGRID_API_KEY'))
            response = sg.send(message)
            print(response.status_code)
            print(response.body)
            print(response.headers)
            return 1, 'sent'
        except Exception as e:
            # print(str(e))
            return 0, str(e.message)



def get_sources():
    import os
    from dotenv import load_dotenv
    import requests
    from news.models import Source, Country

    load_dotenv()

    url = "https://newsapi.org/v2/sources"
    NEWS_API_KEY = os.environ.get('NEWS_API_KEY')
    querystring = {"apiKey": NEWS_API_KEY}

    payload = ""
    try:
        r = requests.request("GET", url, data=payload, params=querystring)
        if r.status_code == 200:
            status = r.json()['status']
            if status == 'ok':
                for s in r.json()['sources']:
                    source_id = s.get('id')
                    source_name = s.get('name')
                    country_code = s.get('country')
                    # save the source and countries
                    try:
                        source = Source(source_id=source_id, source_name=source_name)
                        source.save()
                    except Exception as e:
                        print('Errors Saving Source: f{source_name}:', e)
                    try:
                        country = Country(country_code=country_code)
                        country.save()
                    except Exception as e:
                        print('Errors Saving Country: f{country_code}:', e)

                    print(s.get('id'), s.get('name'))
    except Exception as e:
        print('Error in Fetching:', str(e))


def fetch_news_by_country_codes():
    '''
    The idea is to look for the countries that have atleast 1 engagement count
    for efficient API calling. 
    :return: 
    '''
    import os
    from dotenv import load_dotenv
    import requests
    from news.models import Country, News

    load_dotenv()

    url = 'https://newsapi.org/v2/top-headlines'
    NEWS_API_KEY = os.environ.get('NEWS_API_KEY')
    querystring = {
        'apiKey': NEWS_API_KEY,
    }
    payload = ""

    for c in Country.objects.filter(engage_count__gt=0):
        print(c.country_code)
        querystring['country'] = c.country_code

        # Process the API Call
        try:
            r = requests.request("GET", url, data=payload, params=querystring)
            if r.status_code == 200:
                status = r.json()['status']
                if status == 'ok':
                    for art in r.json()['articles']:
                        source_id = art['source']['id'] if art['source']['id'] else ''
                        source_name = art['source']['name'] if art['source']['name'] else ''
                        country_code = c.country_code
                        headline = art['title'] if art['title'] else ''
                        url = art['url'] if art['url'] else ''
                        url_to_image = art['urlToImage'] if art['urlToImage'] else ''
                        content = art['content'] if art['content'] else ''
                        try:
                            n = News(
                                source_id=source_id,
                                source_name=source_name,
                                country_code=country_code,
                                headline=headline,
                                url=url,
                                url_to_image=url_to_image,
                                content=content,
                            )
                            n.save()
                        except Exception as e:
                            print('Error Saving News By country:', e)
        except Exception as e:
            print('Error in Fetching:', str(e))


def fetch_news_by_source_ids():
    '''
    The idea is to look for the countries that have atleast 1 engagement count
    for efficient API calling. 
    :return: 
    '''
    import os
    from dotenv import load_dotenv
    import requests
    from news.models import Source, News

    load_dotenv()

    url = 'https://newsapi.org/v2/top-headlines'
    NEWS_API_KEY = os.environ.get('NEWS_API_KEY')
    querystring = {
        'apiKey': NEWS_API_KEY,
    }
    payload = ""

    for s in Source.objects.filter(engage_count__gt=0):
        print(s.source_id)
        querystring['sources'] = s.source_id

        # Process the API Call
        try:
            r = requests.request("GET", url, data=payload, params=querystring)
            if r.status_code == 200:
                status = r.json()['status']
                if status == 'ok':
                    for art in r.json()['articles']:
                        source_id = art['source']['id'] if art['source']['id'] else ''
                        source_name = art['source']['name'] if art['source']['name'] else ''
                        country_code = ''
                        headline = art['title'] if art['title'] else ''
                        url = art['url'] if art['url'] else ''
                        url_to_image = art['urlToImage'] if art['urlToImage'] else ''
                        content = art['content'] if art['content'] else ''
                        try:
                            n = News(
                                source_id=source_id,
                                source_name=source_name,
                                country_code=country_code,
                                headline=headline,
                                url=url,
                                url_to_image=url_to_image,
                                content=content,
                            )
                            n.save()
                        except Exception as e:
                            print('Error Saving News By country:', e)
        except Exception as e:
            print('Error in Fetching:', str(e))


            # get_sources()



def scan_for_newsletter():
    from functools import reduce
    import operator
    from django.db.models import Q
    from news.models import NewsPreference, News, Newsletter
    # for each user prefernece check if the user has key-words
    # for each of that keywords, search that in every news
    for nf in NewsPreference.objects.all():
        user = nf.user
        # fetch the news specific to user
        sources = (nf.sources).split(',')
        try:
            sources.remove('')
        except:
            pass
        sources.append('xor-xor-xor')

        countries = (nf.countries).split(',')
        try:
            countries.remove('')
        except:
            pass
        countries.append('xor-xor-xor')
        q1 = reduce(operator.or_, (Q(source_id__contains=s) for s in sources))
        q2 = reduce(operator.or_, (Q(country_code__contains=c) for c in countries))

        try:
            result = News.objects.filter(q1 | q2).all().order_by('-created')
        except Exception as e:
            print('Error Finding Multiple News:', e)
            result = []
        kw = nf.search_words.strip().split(',')
        for n in result:
            flag = False
            title = n.headline
            for k in kw:
                if k=='':
                    continue
                if k in title:
                    flag = True
                    break
            if flag:
                try:
                    newsletter,_ = Newsletter.objects.get_or_create(user=user,news=n)
                    if newsletter.is_sent:
                        newsletter.allowed = False
                        newsletter.save()
                except Exception as e:
                    print ('Error in Saving Newsletter:', e)


def send_email():
    mail_util = MailUtil()
    subject = 'Newsmonger Newsletter'

    head_template = '''
    <h4>{}</h4><a href="{}"></a>
    '''
    from news.models import Newsletter
    newsletters = Newsletter.objects.filter(allowed=True,is_sent=False)
    for nl in newsletters:

        html_content = head_template.format(nl.news.headline,nl.news.url)
        to_mails = '{}'.format(nl.user.email)

        sts, msg = mail_util.send_mail(user_mail=to_mails,subject=subject,html_content=html_content)
        print (sts, msg)
        if sts:
            nl.is_sent=True
            nl.allowed = False
            nl.save()


# scan_for_newsletter()
# send_email()

