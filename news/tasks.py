# -*- coding: utf-8 -*-

"""
author: S.M. Sabbir Amin
date: 27 Jun 2021
email: sabbiramin.cse11ruet@gmail.com, sabbir@rokomari.com

"""
from news.utils import fetch_news_by_country_codes, fetch_news_by_source_ids, scan_for_newsletter, send_email


def fetching_news():
    fetch_news_by_country_codes()
    fetch_news_by_source_ids()

def scanning_keys_for_newsletter():
    scan_for_newsletter()

def task_send_newsletter_mail():
    send_email()