# -*- coding: utf-8 -*-

"""
author: S.M. Sabbir Amin
date: 27 Jun 2021
email: sabbiramin.cse11ruet@gmail.com, sabbir@rokomari.com

"""
import os
from apscheduler.schedulers.background import BackgroundScheduler
from news.tasks import fetching_news, scanning_keys_for_newsletter,\
    task_send_newsletter_mail
from dotenv import load_dotenv
def start_job():
    load_dotenv()
    fetching_news_interval = int(os.environ.get('fetching_news_interval'))
    scanning_news_interval = int(os.environ.get('scanning_news_interval'))
    sending_mail_interval = int(os.environ.get('sending_mail_interval'))




    fetching_scheduler = BackgroundScheduler()
    #Start fetching News
    fetching_scheduler.add_job(fetching_news,
                               "interval",
                               minutes=fetching_news_interval,
                               id='fetching_task',
                               replace_existing=True)

    # Start Scanning for preparing newsletter
    fetching_scheduler.add_job(scanning_keys_for_newsletter,
                               "interval",
                               minutes=scanning_news_interval,
                               id='scanning_keys_task',
                               replace_existing=True)

    # Sending Email
    fetching_scheduler.add_job(task_send_newsletter_mail,
                               "interval",
                               hours=sending_mail_interval,
                               id='sending_mail_task',
                               replace_existing=True)

    fetching_scheduler.start()
