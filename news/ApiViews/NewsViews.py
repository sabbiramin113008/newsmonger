# -*- coding: utf-8 -*-

"""
author: S.M. Sabbir Amin
date: 27 Jun 2021
email: sabbiramin.cse11ruet@gmail.com, sabbir@rokomari.com

"""
import operator
from django.db.models import Q
from django.http import JsonResponse
from rest_framework.response import Response
from rest_framework import permissions, status
from rest_framework.views import APIView

from functools import reduce

from news.models import NewsPreference, Country, Source, News
from news.serializers import NewsPreferenceSerializer, NewsSerializer


def sample_view(request):
    response = {
        'hello': 'world'
    }
    return JsonResponse(response)


class DetailsNewsPreference(APIView):
    permission_classes = [permissions.IsAuthenticated]

    def get(self, request):
        user = request.user
        nf = NewsPreference.objects.filter(user=user).first()
        response = dict()
        if not nf:
            response['status'] = 'error'
            response['model'] = None
            return Response(response, status=status.HTTP_204_NO_CONTENT)
        serializer = NewsPreferenceSerializer(nf)
        response['status'] = 'success'
        response['model'] = serializer.data
        return Response(response)

    def post(self, request):
        user = request.user
        data = request.data
        news_pref, _ = NewsPreference.objects.get_or_create(user=user)
        countries = data.get('countries', '').strip().split(',')
        sources = data.get('sources', '').strip().split(',')
        # Set individual countries and sources engage count in the db
        ncountries = []
        nsources = []
        for c in countries:
            try:
                mc = Country.objects.get(country_code__iexact=c)
                ncountries.append(mc.country_code)
                mc_count = mc.engage_count + 1
                mc.engage_count = mc_count
                mc.save()
            except:
                pass

        for s in sources:
            try:
                sc = Source.objects.get(source_id__iexact=s)
                nsources.append(sc.source_id)
                sc_count = sc.engage_count + 1
                sc.engage_count = sc_count
                sc.save()
            except:
                pass

        news_pref.countries = ','.join(ncountries)
        news_pref.sources = ','.join(nsources)
        news_pref.search_words = data.get('search_words')
        news_pref.save()
        serializer = NewsPreferenceSerializer(news_pref)
        response = dict()
        response['status'] = 'updated'
        response['model'] = serializer.data
        return Response(response)


class NewsViews(APIView):
    permission_classes = [permissions.IsAuthenticated]

    def get(self, request):
        user = request.user

        print('user:', user.username)
        news = []
        response = dict()
        # load user's preference
        try:
            nf = NewsPreference.objects.get(user=user)
        except:
            nf = None
        if not nf:
            response['models'] = news
            return Response(response, status=status.HTTP_204_NO_CONTENT)
        sources = (nf.sources).split(',')
        try:
            sources.remove('')
        except:
            pass
        sources.append('xor-xor-xor')

        countries = (nf.countries).split(',')
        try:
            countries.remove('')
        except:
            pass
        countries.append('xor-xor-xor')
        q1 = reduce(operator.or_, (Q(source_id__contains=s) for s in sources))
        q2 = reduce(operator.or_, (Q(country_code__contains=c) for c in countries))

        try:
            result = News.objects.filter(q1 | q2).all().order_by('-created')
            serializer = NewsSerializer(result, many=True)
            response['models'] = serializer.data
            return Response(response)
        except Exception as e:
            print('Error Finding Multiple News:', e)
            response['models'] = news
            return Response(response, status=status.HTTP_204_NO_CONTENT)


class NewsDetails(APIView):
    permission_classes = [permissions.IsAuthenticated]

    def get(self, request, *args, **kwargs):
        response = dict()
        news = News.objects.filter(pk=kwargs.get('pk')).first()
        if not news:
            response['model'] = None
            return Response(response, status=status.HTTP_204_NO_CONTENT)
        serializer = NewsSerializer(news, many=False)
        response['model'] = serializer.data
        return Response(response)
