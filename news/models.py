# -*- coding: utf-8 -*-

"""
author: S.M. Sabbir Amin
date: 26 Jun 2021
email: sabbiramin.cse11ruet@gmail.com, sabbir@rokomari.com

"""
from django.db import models
from django.contrib.auth import get_user_model
from django.db import models


# Todo:// Add script for pre-fill the country and source from the API Call

class Country(models.Model):
    country_code = models.CharField(null=False, unique=True, max_length=10)
    country_name = models.CharField(null=True, max_length=20)
    engage_count = models.IntegerField(default=0)

    def __str__(self):
        return self.country_code


class Source(models.Model):
    source_id = models.CharField(null=False, unique=True, max_length=10)
    source_name = models.CharField(null=False, unique=True, max_length=20)
    engage_count = models.IntegerField(default=0)

    def __str__(self):
        return self.source_id


class News(models.Model):
    source_id = models.CharField(default='', max_length=10)
    source_name = models.CharField(default='', max_length=20)
    country_code = models.CharField(default='', max_length=10)
    country_name = models.CharField(default='', max_length=20)
    headline = models.CharField(max_length=255, null=False, unique=True)
    url = models.TextField(null=False)
    url_to_image = models.TextField(null=True)
    content = models.TextField(null=True)
    created = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.headline[:10]


# UserModel = get_user_model()


# Todo:// Add valid countries and sources so that user can not enter invalid ones.
class NewsPreference(models.Model):
    user = models.OneToOneField(get_user_model(), null=True, on_delete=models.CASCADE)
    countries = models.TextField(max_length=1024, default='us')  # separed by comma
    sources = models.TextField(max_length=1024, default='fox-news')  # separated by comma
    search_words = models.TextField(max_length=1024, null=True, blank=True)

    def __str__(self):
        return self.user.username


class Newsletter(models.Model):
    user = models.ForeignKey(get_user_model(), null=False, on_delete=models.CASCADE)
    news = models.ForeignKey(News, null=False, on_delete=models.CASCADE)
    is_sent = models.BooleanField(default=False)
    allowed = models.BooleanField(default=True)
    created = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '{} - {}'.format(self.user.username, self.news.headline)
