from django.apps import AppConfig

from news.crons import start_job
from news.utils import get_sources, fetch_news_by_country_codes, fetch_news_by_source_ids


class NewsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'news'

    def ready(self):
        print ('....Starting the Loading of Sources...')

        # Temporary posponding the API call for fetching sources
        get_sources()

        #uncomment and start the cronjob here
        from .crons import start_job
        start_job()