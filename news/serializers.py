# -*- coding: utf-8 -*-

"""
author: S.M. Sabbir Amin
date: 27 Jun 2021
email: sabbiramin.cse11ruet@gmail.com, sabbir@rokomari.com

"""
from rest_framework import serializers

from news.models import NewsPreference, News


class NewsPreferenceSerializer(serializers.ModelSerializer):
    class Meta:
        model = NewsPreference
        fields = ['id', 'countries', 'sources', 'search_words']


class NewsSerializer(serializers.ModelSerializer):
    class Meta:
        model = News
        fields = ['id', 'source_id', 'source_name', 'country_code',
                  'headline', 'url', 'url_to_image', 'content'
                  ]
