# -*- coding: utf-8 -*-

"""
author: S.M. Sabbir Amin
date: 26 Jun 2021
email: sabbiramin.cse11ruet@gmail.com, sabbir@rokomari.com

"""
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.shortcuts import render, redirect
from django.contrib.auth import get_user_model
from django.views import View

from functools import reduce
import operator
from django.db.models import Q

from news.models import NewsPreference, Country, Source, News

UserModel = get_user_model()


def index(request):
    context = {}
    return render(request, 'news/hello.html', context)


class Home(View):
    def get(self, request):
        return render(request, 'home.html')


class Login(View):
    def get(self, request):
        return render(request, template_name='users/login.html')

    def post(self, request, *args, **kwargs):
        data = request.POST
        user = authenticate(request, username=data.get('username'), password=data.get('password'))
        if user:
            login(request, user)
            messages.success(request, "Logged in Successfully")
            return redirect('home')
        else:
            messages.error(request, "The provided username or  password is wrong!!")
            return redirect('login')


class LogOut(View):
    def get(self, request):
        user = request.user
        if user:
            logout(request)
            messages.success(request, "Logged Out Successfully!!")

            return redirect('login')
        else:
            messages.error(request, "Something went wrong!!")
            return redirect('home')


class Register(View):
    def get(self, request):
        return render(request, 'users/register.html')

    def post(self, request, *args, **kwargs):
        data = request.POST
        if UserModel.objects.filter(email=data.get('email')).exists():
            messages.error(request, "The email is already Taken!!")
            return redirect('register')
        if UserModel.objects.filter(username=data.get('username')).exists():
            messages.error(request, "The Username is already Taken!!")
            return redirect('register')
        user = UserModel.objects.create_user(data.get('username'), data.get('email'), data.get('password'))
        if user:
            messages.success(request, "Registered Successfully, Please login!!")
            return redirect('login')
        else:
            messages.error(request, "Something went wrong!!")
            return redirect('register')


class CreateNewsPreference(View):
    def get(self, request):
        user = request.user
        print(user.username)
        try:
            news_pref = NewsPreference.objects.filter(user=user).first()
        except UserModel.DoesNotExist:
            news_pref = None
        if news_pref:
            context = {
                'news_pref': news_pref
            }
        else:
            context = {'news_pref': None}
        return render(request, 'news/news_preference.html', context=context)

    def post(self, request):
        user = request.user
        data = request.POST
        print('data:', data)

        news_pref, _ = NewsPreference.objects.get_or_create(user=user)
        countries = data.get('countries', '').strip().split(',')
        sources = data.get('sources', '').strip().split(',')
        # Set individual countries and sources engage count in the db
        ncountries = []
        nsources = []
        for c in countries:
            try:
                mc = Country.objects.get(country_code__iexact=c)
                ncountries.append(mc.country_code)
                mc_count = mc.engage_count + 1
                mc.engage_count = mc_count
                mc.save()
            except:
                pass

        for s in sources:
            try:
                sc = Source.objects.get(source_id__iexact=s)
                nsources.append(sc.source_id)
                sc_count = sc.engage_count + 1
                sc.engage_count = sc_count
                sc.save()
            except:
                pass

        news_pref.countries = ','.join(ncountries)
        news_pref.sources = ','.join(nsources)
        news_pref.search_words = data.get('search_words')
        news_pref.save()
        return redirect('home')


class ListNews(View):
    def get(self, request):
        user = request.user
        news = []
        # load user's preference
        try:
            nf = NewsPreference.objects.get(user=user)
        except:
            nf = None
        if not nf:
            return render(request, 'news/list.html', context={'news': news})
        sources = (nf.sources).split(',')
        try:
            sources.remove('')
        except:
            pass
        sources.append('xor-xor-xor')

        countries = (nf.countries).split(',')
        try:
            countries.remove('')
        except:
            pass

        countries.append('xor-xor-xor')

        print(sources, countries)

        q1 = reduce(operator.or_, (Q(source_id__contains=s) for s in sources))
        q2 = reduce(operator.or_, (Q(country_code__contains=c) for c in countries))

        try:
            result = News.objects.filter(q1 | q2).all().order_by('-created')
            return render(request, 'news/list.html', context={'news': result})
        except Exception as e:
            print('Error Finding Multiple News:', e)
            return render(request, 'news/list.html', context={'news': news})


class DetailNews(View):
    def get(self, request, *args, **kwargs):
        user = request.user
        news = News.objects.filter(id=kwargs.get('id')).first()
        if not news:
            return redirect('list_news')
        return render(request, 'news/view.html', context={'news': news})
