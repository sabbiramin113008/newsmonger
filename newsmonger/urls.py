# -*- coding: utf-8 -*-

"""
author: S.M. Sabbir Amin
date: 26 Jun 2021
email: sabbiramin.cse11ruet@gmail.com, sabbir@rokomari.com

"""
from django.contrib import admin
from django.contrib.auth.decorators import login_required
from django.urls import path
from django.views.decorators.csrf import csrf_exempt


from news.ApiViews.NewsViews import sample_view, DetailsNewsPreference, NewsViews, NewsDetails
from news.views import Login, Register, LogOut, Home, CreateNewsPreference, ListNews, DetailNews


from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)

urlpatterns = [



    path('admin/', admin.site.urls),
    path('', Home.as_view(), name='home'),
    path('login/', csrf_exempt(Login.as_view()), name='login'),
    path('register/', csrf_exempt(Register.as_view()), name='register'),
    path('logout/', login_required(LogOut.as_view()), name='logout'),
    path('preference/', csrf_exempt(CreateNewsPreference.as_view()), name='settings'),
    path('list/', login_required(ListNews.as_view()), name='list_news'),
    path('view/<int:id>', login_required(csrf_exempt(DetailNews.as_view())), name='view_news'),

    # API Settings
    # path('auth/', include('rest_auth.urls')),
    # path('auth/registration/', include('rest_auth.registration.urls'))
    path('api/token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('api/token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),

    path('api/v1/sample', sample_view, name='sample_api'),
    path('api/v1/settings/', DetailsNewsPreference.as_view(), name='Settings API'),
    path('api/v1/news/', NewsViews.as_view(), name='News API'),
    path('api/v1/news/<int:pk>', NewsDetails.as_view(), name='Details News API'),


]

